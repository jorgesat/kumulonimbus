---
title: "Hvis regnskogen i Amazonas dør, vil enorme mengder karbon slippes ut i atmosfæren"
date: 2019-02-22
author:
  - name: Jorge Saturno
    url: 
    orcid: 0000-0002-3761-3957
    affiliation: Max Planck Institute for Chemistry
categories: 
  - Amazon rainforest
  - Black carbon
  - Atmospheric aerosols
pub-info: 
  reference: >-
    <a>Jorge Saturno</a>,
    <strong>Hvis regnskogen i Amazonas dør, vil enorme mengder karbon slippes ut i atmosfæren</strong>, <em>Forskning</em>, 2019.
  links: 
    - name: Leer artículo
      url: https://www.forskning.no/forskere-fra-sor-klima-kronikk/hvis-regnskogen-i-amazonas-dor-vil-enorme-mengder-karbon-slippes-ut-i-atmosfaeren/1295721
      icon: fa-solid fa-arrow-up-right-from-square
    - name: Agregar a Zotero
      url: https://www.zotero.org/save?type=url&q=https://www.forskning.no/forskere-fra-sor-klima-kronikk/hvis-regnskogen-i-amazonas-dor-vil-enorme-mengder-karbon-slippes-ut-i-atmosfaeren/1295721
      icon: ai ai-zotero
---