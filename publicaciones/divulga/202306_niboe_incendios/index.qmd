---
title: "Partículas provenientes de incendios forestales están alcanzando la estratosfera"
date: 2023-06-14
author:
  - name: Jorge Saturno
    url: 
    orcid: 0000-0002-3761-3957
    affiliation: Physikalisch-Technische Bundesanstalt
categories: 
  - Biomass burning
  - Atmospheric aerosols
pub-info: 
  reference: >-
    <a>Jorge Saturno</a>,
    <strong>Partículas provenientes de incendios forestales están alcanzando la estratosfera</strong>, <em>Niboe</em>, 2023.
  links: 
    - name: Leer artículo
      url: https://niboe.info/particulas-de-incendios-forestales-alcanzan-estratosfera/
      icon: fa-solid fa-arrow-up-right-from-square
    - name: Agregar a Zotero
      url: https://www.zotero.org/save?type=url&q=https://niboe.info/particulas-de-incendios-forestales-alcanzan-estratosfera
      icon: ai ai-zotero
---