---
title: "📄 Publicaciones"
listing:
  - id: reviewed
    contents: "reviewed/*/index.qmd"
    sort: "date desc"
    template: ../html/publicaciones/listing.ejs
    categories: true
  # - id: pre-prints
  #   contents: "pre-prints/*/index.qmd"
  #   sort: "date desc"
  #   template: ../html/publicaciones/listing.ejs
  - id: reportes
    contents: "reportes/*/index.qmd"
    sort: "date desc"
    template: ../html/publicaciones/listing.ejs
    categories: true
  - id: divulgacion
    contents: "divulga/*/index.qmd"
    sort: "date desc"
    template: ../html/publicaciones/listing_justLink.ejs
page-layout: full
title-block-banner: "#004e66"
include-back-link: false
toc-location: right
---

Artículos relacionados con mi investigación y otros temas que me interesan profesionalmente.


## Artículos arbitrados

:::{#reviewed}
:::


<!-- ## Pre-prints -->

<!-- :::{#pre-prints} -->
<!-- ::: -->


## Reportes

:::{#reportes}
:::


## Artículos de divulgación

:::{#divulgacion}
:::