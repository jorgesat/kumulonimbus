---
title: "Soluble iron nutrients in Saharan dust over the central Amazon rainforest"
date: 2017-01-01
author:
  - name: Joana A Rizzolo
    url: 
    orcid: 
    affiliation: 
  - name: Cybelli G G Barbosa
    url: 
    orcid: 
    affiliation: 
  - name: Guilherme C Borillo
    url: 
    orcid: 
    affiliation: 
  - name: Ana F L Godoi
    url: 
    orcid: 
    affiliation: 
  - name: Rodrigo A F Souza
    url: 
    orcid: 
    affiliation: 
  - name: Rita V Andreoli
    url: 
    orcid: 
    affiliation: 
  - name: Eliane G Alves
    url: 
    orcid: 
    affiliation: 
  - name: Isabella H Angelis
    url: 
    orcid: 
    affiliation: 
  - name: Florian Ditas
    url: 
    orcid: 
    affiliation: 
  - name: Jorge Saturno
    url: 
    orcid: 
    affiliation: 
  - name: Daniel Moran-Zuloaga
    url: 
    orcid: 
    affiliation: 
  - name: Luciana V Rizzo
    url: 
    orcid: 
    affiliation: 
  - name: Theotonio Pauliquevis
    url: 
    orcid: 
    affiliation: 
  - name: Rosa M N Santos
    url: 
    orcid: 
    affiliation: 
  - name: Carlos I Yamamoto
    url: 
    orcid: 
    affiliation: 
  - name: Meinrat O Andreae
    url: 
    orcid: 
    affiliation: 
  - name: Paulo Artaxo
    url: 
    orcid: 
    affiliation: 
  - name: Philip E Taylor
    url: 
    orcid: 
    affiliation: 
  - name: Ricardo H M Godoi
    url: 
    orcid: 
    affiliation: 
categories: 
  - Amazon rainforest
  - Atmospheric aerosols
  - Dust transport
pub-info: 
  reference: >-
    <a>Joana A Rizzolo</a>,
    <a>Cybelli G G Barbosa</a>,
    <a>Guilherme C Borillo</a>,
    <a>Ana F L Godoi</a>,
    <a>Rodrigo A F Souza</a>,
    <a>Rita V Andreoli</a>,
    <a>Eliane G Alves</a>,
    <a>Isabella H Angelis</a>,
    <a>Florian Ditas</a>,
    <a>Jorge Saturno</a>,
    <a>Daniel Moran-Zuloaga</a>,
    <a>Luciana V Rizzo</a>,
    <a>Theotonio Pauliquevis</a>,
    <a>Rosa M N Santos</a>,
    <a>Carlos I Yamamoto</a>,
    <a>Meinrat O Andreae</a>,
    <a>Paulo Artaxo</a>,
    <a>Philip E Taylor</a>,
    <a>Ricardo H M Godoi</a>,
    <strong>Soluble iron nutrients in Saharan dust over the central Amazon rainforest</strong>, <em>Atmospheric Chemistry and Physics</em> 17: 2673--2687, doi: <a href=https://doi.org/10.5194/acp-17-2673-2017><code>10.5194/acp-17-2673-2017</code></a>
  links: 
    - name: Versión final
      url: https://doi.org/10.5194/acp-17-2673-2017
      icon: fa-solid fa-scroll
    - name: Agregar a Zotero
      url: https://www.zotero.org/save?type=doi&q=10.5194/acp-17-2673-2017
      icon: ai ai-zotero
  extra: >-
    [insert extra comment]
doi: 10.5194/acp-17-2673-2017
haiku: 
  - Llegada de nutrientes
  - desde África hacia el Amazonas
---

## Abstract

The intercontinental transport of aerosols from the Sahara desert plays a significant role in nutrient cycles in the Amazon rainforest, since it carries many types of minerals to these otherwise low-fertility lands. Iron is one of the micronutrients essential for plant growth, and its long-range transport might be an important source for the iron-limited Amazon rainforest. This study assesses the bioavailability of iron Fe(II) and Fe(III) in the particulate matter over the Amazon forest, which was transported from the Sahara desert (for the sake of our discussion, this term also includes the Sahel region). The sampling campaign was carried out above and below the forest canopy at the ATTO site (Amazon Tall Tower Observatory), a near-pristine area in the central Amazon Basin, from March to April 2015. Measurements reached peak concentrations for soluble Fe(III) (48 ng m−3), Fe(II) (16 ng m−3), Na (470 ng m−3), Ca (194 ng m−3), K (65 ng m−3), and Mg (89 ng m−3) during a time period of dust transport from the Sahara, as confirmed by ground-based and satellite remote sensing data and air mass backward trajectories. Dust sampled above the Amazon canopy included primary biological aerosols and other coarse particles up to 12 µm in diameter. Atmospheric transport of weathered Saharan dust, followed by surface deposition, resulted in substantial iron bioavailability across the rainforest canopy. The seasonal deposition of dust, rich in soluble iron, and other minerals is likely to assist both bacteria and fungi within the topsoil and on canopy surfaces, and especially benefit highly bioabsorbent species. In this scenario, Saharan dust can provide essential macronutrients and micronutrients to plant roots, and also directly to plant leaves. The influence of this input on the ecology of the forest canopy and topsoil is discussed, and we argue that this influence would likely be different from that of nutrients from the weathered Amazon bedrock, which otherwise provides the main source of soluble mineral nutrients.

## Citation

```bibtex
@article{Rizzolo2017,
 author = {Rizzolo, Joana A and Barbosa, Cybelli G G and Borillo, Guilherme C and Godoi, Ana F L and Souza, Rodrigo A F and Andreoli, Rita V and Alves, Eliane G and Angelis, Isabella H and Ditas, Florian and Saturno, Jorge and Moran-Zuloaga, Daniel and Rizzo, Luciana V and Pauliquevis, Theotonio and Santos, Rosa M N and Yamamoto, Carlos I and Andreae, Meinrat O and Artaxo, Paulo and Taylor, Philip E and Godoi, Ricardo H M},
 doi = {10.5194/acp-17-2673-2017},
 journal = {Atmospheric Chemistry and Physics},
 keywords = {ATTO,Amazon,Dust,Dust transport},
 pages = {2673--2687},
 title = {Soluble iron nutrients in Saharan dust over the central Amazon rainforest},
 volume = {17},
 year = {2017}
}
```

