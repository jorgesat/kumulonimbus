---
title: "African volcanic emissions influencing atmospheric aerosols over the Amazon rain forest"
date: 2018-01-01
author:
  - name: Jorge Saturno
    url: 
    orcid: 
    affiliation: 
  - name: Florian Ditas
    url: 
    orcid: 
    affiliation: 
  - name: Marloes Penning de Vries
    url: 
    orcid: 
    affiliation: 
  - name: Bruna A Holanda
    url: 
    orcid: 
    affiliation: 
  - name: Mira L. Pöhlker
    url: 
    orcid: 
    affiliation: 
  - name: Samara Carbone
    url: 
    orcid: 
    affiliation: 
  - name: David Walter
    url: 
    orcid: 
    affiliation: 
  - name: Nicole Bobrowski
    url: 
    orcid: 
    affiliation: 
  - name: Joel Brito
    url: 
    orcid: 
    affiliation: 
  - name: Xuguang Chi
    url: 
    orcid: 
    affiliation: 
  - name: Alexandra Gutmann
    url: 
    orcid: 
    affiliation: 
  - name: Isabella Hrabe de Angelis
    url: 
    orcid: 
    affiliation: 
  - name: Luiz A T Machado
    url: 
    orcid: 
    affiliation: 
  - name: Daniel Moran-Zuloaga
    url: 
    orcid: 
    affiliation: 
  - name: Julian Rüdiger
    url: 
    orcid: 
    affiliation: 
  - name: Johannes Schneider
    url: 
    orcid: 
    affiliation: 
  - name: Christiane Schulz
    url: 
    orcid: 
    affiliation: 
  - name: Qiaoqiao Wang
    url: 
    orcid: 
    affiliation: 
  - name: Manfred Wendisch
    url: 
    orcid: 
    affiliation: 
  - name: Paulo Artaxo
    url: 
    orcid: 
    affiliation: 
  - name: Thomas Wagner
    url: 
    orcid: 
    affiliation: 
  - name: Ulrich Pöschl
    url: 
    orcid: 
    affiliation: 
  - name: Meinrat O Andreae
    url: 
    orcid: 
    affiliation: 
  - name: Christopher Pöhlker
    url: 
    orcid: 
    affiliation: 
categories: 
  - Atmospheric aerosols
  - Volcanic emissions
  - Amazon rainforest
  - Satellite measurements
  - Cloud condensation nuclei
pub-info: 
  reference: >-
    <a>Jorge Saturno</a>,
    <a>Florian Ditas</a>,
    <a>Marloes Penning de Vries</a>,
    <a>Bruna A Holanda</a>,
    <a>Mira L. Pöhlker</a>,
    <a>Samara Carbone</a>,
    <a>David Walter</a>,
    <a>Nicole Bobrowski</a>,
    <a>Joel Brito</a>,
    <a>Xuguang Chi</a>,
    <a>Alexandra Gutmann</a>,
    <a>Isabella Hrabe de Angelis</a>,
    <a>Luiz A T Machado</a>,
    <a>Daniel Moran-Zuloaga</a>,
    <a>Julian Rüdiger</a>,
    <a>Johannes Schneider</a>,
    <a>Christiane Schulz</a>,
    <a>Qiaoqiao Wang</a>,
    <a>Manfred Wendisch</a>,
    <a>Paulo Artaxo</a>,
    <a>Thomas Wagner</a>,
    <a>Ulrich Pöschl</a>,
    <a>Meinrat O Andreae</a>,
    <a>Christopher Pöhlker</a>,
    <strong>African volcanic emissions influencing atmospheric aerosols over the Amazon rain forest</strong>, <em>Atmospheric Chemistry and Physics</em> 18: 10391--10405, doi: <a href=https://doi.org/10.5194/acp-18-10391-2018><code>10.5194/acp-18-10391-2018</code></a>
  links: 
    - name: Versión final
      url: https://doi.org/10.5194/acp-18-10391-2018
      icon: fa-solid fa-scroll
    - name: Agregar a Zotero
      url: https://www.zotero.org/save?type=doi&q=10.5194/acp-18-10391-2018
      icon: ai ai-zotero
  extra: >-
    Tracking volcanic emissions transported from Africa to the Amazon rainforest.
doi: 10.5194/acp-18-10391-2018
haiku: 
  - Cuando emisiones volcánicas de África
  - viajaron sobre el Océano Atlántico
  - y llegaron al corazón del Amazonas
---

## Abstract
The long-range transport (LRT) of trace gases and aerosol particles plays an important role for the composition of the Amazonian rain forest atmosphere. Sulfate aerosols originate to a substantial extent from LRT sources and play an important role in the Amazonian atmosphere as strongly light-scattering particles and effective cloud condensation nuclei. The transatlantic transport of volcanic sulfur emissions from Africa has been considered as a source of particulate sulfate in the Amazon; however, direct observations have been lacking so far. This study provides observational evidence for the influence of emissions from the Nyamuragira–Nyiragongo volcanoes in Africa on Amazonian aerosol properties and atmospheric composition during September 2014. Comprehensive ground-based and airborne aerosol measurements together with satellite observations are used to investigate the volcanic event. Under the volcanic influence, hourly mean sulfate mass concentrations in the submicron size range reached up to 3.6µgm−3 at the Amazon Tall Tower Observatory, the highest value ever reported in the Amazon region. The substantial sulfate injection increased the aerosol hygroscopicity with $ąppa$ values up to 0.36, thus altering aerosol–cloud interactions over the rain forest. Airborne measurements and satellite data indicate that the transatlantic transport of volcanogenic aerosols occurred in two major volcanic plumes with a sulfate-enhanced layer between 4 and 5km of altitude. This study demonstrates how African aerosol sources, such as volcanic sulfur emissions, can substantially affect the aerosol cycling and atmospheric processes in Amazonia.

## Citation

```bibtex
@article{Saturno2018a,
 author = {Saturno, Jorge and Ditas, Florian and Penning de Vries, Marloes and Holanda, Bruna A and Pöhlker, Mira L. and Carbone, Samara and Walter, David and Bobrowski, Nicole and Brito, Joel and Chi, Xuguang and Gutmann, Alexandra and Hrabe de Angelis, Isabella and Machado, Luiz A T and Moran-Zuloaga, Daniel and Rüdiger, Julian and Schneider, Johannes and Schulz, Christiane and Wang, Qiaoqiao and Wendisch, Manfred and Artaxo, Paulo and Wagner, Thomas and Pöschl, Ulrich and Andreae, Meinrat O and Pöhlker, Christopher},
 doi = {10.5194/acp-18-10391-2018},
 issn = {1680-7324},
 journal = {Atmospheric Chemistry and Physics},
 keywords = {Amazon,Volcanic emissions},
 month = {jul},
 number = {14},
 pages = {10391--10405},
 title = {African volcanic emissions influencing atmospheric aerosols over the Amazon rain forest},
 url = {https://www.atmos-chem-phys.net/18/10391/2018/},
 volume = {18},
 year = {2018}
}

```
